# OR-JoinPreprocessing

This repository contains the preprocessing class needed for the BPMN 2.0 or-join semantics implementation.

## Usage

An example usage is available <code>src/test/java/TestPreprocessing.java</code>. To use custom BPMN model, please modify line 14:

```java
BpmnModelInstance mi = Bpmn.readModelFromFile(new File("path_to_BPMN_file"));