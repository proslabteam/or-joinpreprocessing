import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.FlowNode;
import org.camunda.bpm.model.bpmn.instance.InclusiveGateway;
import org.camunda.bpm.model.bpmn.instance.Process;
import org.camunda.bpm.model.bpmn.instance.SequenceFlow;

import java.io.File;
import java.util.*;

public class LocalvsGlobal {

    public static void main(String[] args){

        Set<String> incoming = new HashSet<>();
        Map<String, Integer> sigma = new HashMap<>();
        Map<String, Boolean> noDep = new HashMap<>();
        List<List<String>> paths = new ArrayList<>();
        String e = "";
        BpmnModelInstance mi = Bpmn.readModelFromFile(new File("example.bpmn"));
        for (Process process : mi.getModelElementsByType(Process.class)) {
            Preprocessing preprocessing = new Preprocessing(process);
            for (InclusiveGateway or : process.getChildElementsByType(InclusiveGateway.class)) {
                e = or.getOutgoing().iterator().next().getId();
                if (or.getIncoming().size() > 1 && or.getOutgoing().size() == 1) {
                    for (SequenceFlow inc : or.getIncoming()){
                        incoming.add(inc.getId());
                    }
                    noDep.put(e, preprocessing.getDep(or).isEmpty());
                    for (List<SequenceFlow> p : preprocessing.getP(or)){
                        List<String> tmp = new ArrayList<>();
                        for (SequenceFlow s : p){
                            tmp.add(s.getName());
                        }
                        paths.add(tmp);
                    }
                }
                break;
            }
            break;
        }

//        for (SequenceFlow sF : mi.getModelElementsByType(SequenceFlow.class)){
//            sigma.put(sF.getId(), Math.random()>0.5 ? 1 : 0);
//        }
//        for (int i = 0; i < 100; i++){
//            sigma.put("sF"+i, Math.random()>0.5 ? 1 : 0);
//            noDep.put("sF"+i, Math.random()>0.8);
//        }
//
//        List<String> alphabet = new ArrayList<>(sigma.keySet());
//        incoming.add(alphabet.get((int) (Math.random()*alphabet.size())));
//        incoming.add(alphabet.get((int) (Math.random()*alphabet.size())));
//
//        for (int i = 0; i< Math.random()*20; i++){
//            List<String> p = new ArrayList<>();
//            for (int j = 0; j < Math.random()*50; j++){
//                p.add(alphabet.get((int) (Math.random()*alphabet.size())));
//            }
//        }

        long localTime, globalTime;
        localTime = System.nanoTime();
        isActiveLocal(e,incoming,noDep);
        isActiveLocal(e,incoming,noDep);
        localTime = System.nanoTime() - localTime;
        globalTime = System.nanoTime();
        for(int rep = 0; rep<10002; rep++){
            isActiveGlobal(e,incoming, sigma, paths);
        }
        globalTime =  System.nanoTime() - globalTime;
        System.out.println(localTime+ " +++ "+ globalTime);

    }


    private static boolean isActiveLocal(String e, Set<String> incoming, Map<String, Boolean> noDep){
        for (String e1 : incoming){
            if (isWait(e1)){
                return false;
            }
        }
        for (String e2 : incoming){
            if (isLive(e2) && (isNC(e2) || noDep.get(e2)) ){
                return true;
            }
        }
        return false;
    }

    private static boolean isActiveGlobal(String e, Set<String> incoming, Map<String, Integer> sigma, List<List<String>> paths) {
        boolean hasToken = false;
        for(String e1 : incoming){
            if (sigma.get(e1)>0)
                hasToken = true;
        }
        if (! hasToken){
            return false;
        }
        Set<String> E1 = new HashSet<>(), E2 = new HashSet<>();
        for (List<String> path : paths){
            if (sigma.get(path.get(0)) > 0){
                if (sigma.get(path.size()-1) > 0){
                    E1.add(path.get(0));
                } else {
                    E2.add(path.get(0));
                }
            }
        }
        E1.removeAll(E2);
        if (!E1.isEmpty())
            return true;
        return false;
    }
    private static boolean isNC(String e) {
        return Math.random() > 0.8;
    }

    private static boolean isLive(String e) {
        return Math.random() > 0.8;
    }

    private static boolean isWait(String e) {
        return Math.random() > 0.8;
    }

}
