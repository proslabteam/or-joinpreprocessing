import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.camunda.bpm.model.bpmn.instance.FlowNode;
import org.camunda.bpm.model.bpmn.instance.InclusiveGateway;
import org.camunda.bpm.model.bpmn.instance.Process;
import org.camunda.bpm.model.bpmn.instance.SequenceFlow;
import org.camunda.bpm.model.bpmn.instance.StartEvent;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.cycle.SzwarcfiterLauerSimpleCycles;
import org.jgrapht.alg.shortestpath.AllDirectedPaths;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

/**
 * 
 * This class implements the preprocessing procedure useful for check the
 * Or-Join gateways activation.
 * 
 * @author Lorenzo Rossi
 *
 */
public class Preprocessing {

	private Process process;
	private SimpleDirectedWeightedGraph<FlowNode, DefaultWeightedEdge> graph;
	private FlowNode start;
	private HashMap<FlowNode, Set<FlowNode>> res;
	private HashSet<InclusiveGateway> incLoop;
	private List<List<FlowNode>> cycl;
	private Set<InclusiveGateway> orJoins;
	private Set<SequenceFlow> initDead;

	/**
	 * 
	 * The preprocessing object.
	 * 
	 * @param p
	 *            The BPMN process.
	 */
	public Preprocessing(Process p) {
		this.process = p;
		this.incLoop = new HashSet<InclusiveGateway>();
		this.orJoins = new HashSet<InclusiveGateway>();
		this.initDead = new HashSet<SequenceFlow>();
		Collection<StartEvent> starts = p.getChildElementsByType(StartEvent.class);
		if (starts.size() > 1) {
			throw new IllegalArgumentException("The process contains more than 1 start event.");
		}
		for (StartEvent s : starts) {
			this.start = s;
		}
		this.graph = new SimpleDirectedWeightedGraph<FlowNode, DefaultWeightedEdge>(DefaultWeightedEdge.class);
		for (SequenceFlow e : process.getChildElementsByType(SequenceFlow.class)) {
			graph.addVertex(e.getSource());
			graph.addVertex(e.getTarget());
			graph.addEdge(e.getSource(), e.getTarget());
		}
		findOrJoins();
		findCycles();
		getDependency();
		findInitDead();
	}

	private void findOrJoins() {
		for (InclusiveGateway g : process.getChildElementsByType(InclusiveGateway.class)) {
			if (g.getOutgoing().size() == 1) {
				orJoins.add(g);
			}
		}
	}

	/**
	 * 
	 * Dep is a function that taken as input the considered OR-Join, returns the
	 * set of all other mutually dependent OR-Joins of the one in input.
	 * 
	 * @param orJoin
	 *            The gateway
	 * @return The mutual dependent or-join.
	 */
	public Set<FlowNode> getDep(InclusiveGateway orJoin) {
		return res.get(orJoin);
	}

	private Map<FlowNode, Set<FlowNode>> getDependency() {
		res = new HashMap<FlowNode, Set<FlowNode>>();
		for (FlowNode n : orJoins) {
			res.put(n, new HashSet<FlowNode>());
		}
		List<FlowNode> tmp;
		for (List<FlowNode> cycle : cycl) {
			tmp = new ArrayList<FlowNode>(cycle);
			tmp.retainAll(orJoins);
			for (FlowNode g : tmp) {
				res.get(g).addAll(tmp);
				res.get(g).remove(g);
			}
		}
		return res;
	}

	/**
	 * 
	 * P is a function that, given as input an OR- Join, returns the set of all
	 * possible paths starting from the OR-Join backward to all the possible
	 * vertices till the start that do not visit the considered OR-Join.
	 * 
	 * @param orJoin The considered or-join
	 * @return The set of paths.
	 */
	public Set<List<SequenceFlow>> getP(InclusiveGateway orJoin) {
		List<List<FlowNode>> paths = new ArrayList<List<FlowNode>>(findPath(orJoin));
		List<List<FlowNode>> cycles = cycl;
		List<List<FlowNode>> pathsToAdd = new ArrayList<List<FlowNode>>();

		for (List<FlowNode> path : paths) {

			for (FlowNode node : path) {
				for (List<FlowNode> cycle : cycles) {
					if (cycle.contains(node) && !cycle.contains(orJoin)) {
						pathsToAdd.add(fuse(path, cycle, node));
					}
				}
			}
		}
		paths.addAll(pathsToAdd);
		Set<List<FlowNode>> tmp = new HashSet<List<FlowNode>>();
		for (List<FlowNode> path : paths) {
			tmp.addAll(subPaths(path));
		}
		paths.addAll(tmp);
		Set<List<SequenceFlow>> p = new HashSet<List<SequenceFlow>>();
		for (List<FlowNode> path : paths) {
			p.addAll(getEdgesPath(path));
		}
		return p;
	}

	/**
	 * 
	 * Initiallydead is a function that, given as input a sequence flow, states if
	 * it has to be set as dead in the initial marking.
	 * 
	 * @param sf The considered sequenceflow
	 * @return true if sf is initially dead
	 */
	public boolean initiallyDead(SequenceFlow sf){
		return initDead.contains(sf);
	}

	private void findInitDead(){
		for (InclusiveGateway g : orJoins){
			for (List<FlowNode> cycle : cycl){
				int index = cycle.indexOf(g);
				if (index != -1){
					boolean hasJoin = false;
					for (int i = index+1; i<cycle.size(); i++){
						if(cycle.get(i).getIncoming().size()>1){
							hasJoin = true;
							initDead.add(seqFlowBtw(cycle.get(i-1), cycle.get(i)));
						}
					}
					for (int i = 0; i<index; i++){
						if(cycle.get(i).getIncoming().size()>1){
							hasJoin = true;
							initDead.add(seqFlowBtw(cycle.get(i-1), cycle.get(i)));
						}
					}
					if (!hasJoin){
						initDead.add(seqFlowBtw(cycle.get(index-1), cycle.get(index)));
					}
				}
			}
		}
	}

	private SequenceFlow seqFlowBtw(FlowNode s, FlowNode t){
		for (SequenceFlow sF : s.getOutgoing()){
			if (t.getIncoming().contains(sF)){
				return sF;
			}
		}
		return null;
	}

	private Set<List<SequenceFlow>> getEdgesPath(List<FlowNode> path) {
		Set<List<SequenceFlow>> edgePath = new HashSet<List<SequenceFlow>>();
		edgePath.add(new ArrayList<SequenceFlow>());
		for (int i = 0; i < path.size() - 1; i++) {
			Set<SequenceFlow> sFs = getSequenceFlows(path.get(i), path.get(i + 1));
			Set<List<SequenceFlow>> partialPathToRemove = new HashSet<List<SequenceFlow>>();
			Set<List<SequenceFlow>> partialPathToAdd = new HashSet<List<SequenceFlow>>();
			for (List<SequenceFlow> partialPath : edgePath){
				partialPathToRemove.add(partialPath);
				for (SequenceFlow sf: sFs){
					List<SequenceFlow> newPartial = new ArrayList<SequenceFlow>(partialPath);
					newPartial.add(sf);
					partialPathToAdd.add(newPartial);
				}
			}
			edgePath.removeAll(partialPathToRemove);
			edgePath.addAll(partialPathToAdd);
		}
		return edgePath;
	}

	/**
	 * 
	 * It returns the set of return edges of the model.
	 * 
	 * @return The return edges.
	 */
	public Set<SequenceFlow> getR() {
		Set<SequenceFlow> r = new HashSet<SequenceFlow>();
		for (List<FlowNode> c : cycl) {
			for (int i = 0; i < c.size() - 1; i++) {
				r.addAll(getSequenceFlows(c.get(i), c.get(i + 1)));
			}
			r.addAll(getSequenceFlows(c.get(c.size() - 1), c.get(0)));
		}
		return r;
	}

	private Set<SequenceFlow> getSequenceFlows(FlowNode source, FlowNode target) {
		HashSet<SequenceFlow> ret = new HashSet<SequenceFlow>();

		for (SequenceFlow s : process.getChildElementsByType(SequenceFlow.class)) {
			if (s.getSource().equals(source) && s.getTarget().equals(target)) {
				ret.add(s);
			}
		}
		return ret;
	}

	private List<List<FlowNode>> subPaths(List<FlowNode> path) {
		List<List<FlowNode>> ret = new ArrayList<List<FlowNode>>();
		for (int i = 1; i < path.size() - 1; i++) {
			ret.add(path.subList(i, path.size()));
		}
		return ret;
	}

	private List<List<FlowNode>> findPath(FlowNode e) {
		List<List<FlowNode>> paths = new ArrayList<List<FlowNode>>();
		AllDirectedPaths<FlowNode, DefaultWeightedEdge> p = new AllDirectedPaths<FlowNode, DefaultWeightedEdge>(graph);

		for (GraphPath<FlowNode, DefaultWeightedEdge> gP : p.getAllPaths(start, e, true, null)) {
			List<FlowNode> tmp = new ArrayList<FlowNode>();
			tmp.add(start);
			for (DefaultWeightedEdge edge : gP.getEdgeList()) {
				tmp.add(graph.getEdgeTarget(edge));
			}
			paths.add(tmp);
		}
		for(FlowNode n : e.getSucceedingNodes().list()){
			for (GraphPath<FlowNode, DefaultWeightedEdge> gP : p.getAllPaths(n, e, true, null)) {
				List<FlowNode> tmp = new ArrayList<FlowNode>();
				tmp.add(n);
				for (DefaultWeightedEdge edge : gP.getEdgeList()) {
					tmp.add(graph.getEdgeTarget(edge));
				}
				paths.add(tmp);
				tmp.add(0,e);
				paths.add(tmp);
			}

		} 

		return paths;
	}

	private void findCycles() {
		SzwarcfiterLauerSimpleCycles<FlowNode, DefaultWeightedEdge> f = new SzwarcfiterLauerSimpleCycles<FlowNode, DefaultWeightedEdge>(
				graph);
		cycl = f.findSimpleCycles();
		for (InclusiveGateway g : orJoins) {
			for (List<FlowNode> n : cycl) {
				if (n.contains(g)) {
					incLoop.add(g);
				}
			}
		}
	}

	private List<FlowNode> fuse(List<FlowNode> path, List<FlowNode> cycle, FlowNode n) {
		List<FlowNode> ret = new ArrayList<FlowNode>(path);
		int idx = cycle.indexOf(n);
		int index = path.indexOf(n);
		for (int i = idx; i < cycle.size(); i++) {
			ret.add(index, cycle.get(i));
			index++;
		}
		for (int i = 0; i < idx; i++) {
			ret.add(index, cycle.get(i));
			index++;
		}
		return ret;
	}

}
