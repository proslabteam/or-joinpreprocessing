import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.InclusiveGateway;
import org.camunda.bpm.model.bpmn.instance.Process;
import org.camunda.bpm.model.bpmn.instance.SequenceFlow;

import java.io.File;
import java.util.List;

public class TestPreprocessing {

    public static void main(String[] args){
        //The BPMN model
        BpmnModelInstance mi = Bpmn.readModelFromFile(new File("example.bpmn"));
        //Iterate processes
        for (Process process : mi.getModelElementsByType(Process.class)){
            Preprocessing preprocessing = new Preprocessing(process);
            //Iterate OR-Joins
            for (InclusiveGateway or : process.getChildElementsByType(InclusiveGateway.class)){
                if (or.getIncoming().size()>1 && or.getOutgoing().size() == 1){
                    System.out.println("Considered OR-Join: "+or.getName());
                    System.out.println("Mutually dependent OR-Joins: "+preprocessing.getDep(or));
                    System.out.println("Paths ending in the OR-Join");
                    for(List<SequenceFlow> paths : preprocessing.getP(or)){
                       for (SequenceFlow sF : paths){
                           System.out.print(sF.getName()+" ");
                       }
                       System.out.print("\n");
                    }
                    System.out.println("Return edges (edges in a cycle)");
                    for(SequenceFlow sF : preprocessing.getR()) {
                        System.out.println(sF.getName());
                    }
                }
            }

            //Iterate sequence flows
            System.out.println("Initially dead sequence flows");
            for (SequenceFlow sF : process.getChildElementsByType(SequenceFlow.class)){
                if(preprocessing.initiallyDead(sF)) {
                    System.out.println(sF.getName());
                }
            }
            System.out.println("-------------------------");
        }

    }
}
